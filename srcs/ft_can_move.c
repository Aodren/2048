/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_can_move.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:14:09 by abary             #+#    #+#             */
/*   Updated: 2016/01/31 18:37:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/truc.h"

static void	ft_offset(int *sty, int *stx)
{
	while (*sty % 4)
		--*sty;
	while (*stx % 4)
		--*stx;
	*sty -= 12;
	*stx -= 4;
}

void		ft_print_cant_move(void)
{
	t_printnb st;

	getmaxyx(stdscr, st.y, st.x);
	ft_offset(&st.y, &st.x);
	mvprintw(st.y / 2, st.x / 2 - 27, "__   __                       _ _ ");
	mvprintw(st.y / 2 + 1, st.x / 2 - 27, "\\ \\ / /__ _  _   __ __ _ _ _ ( )");
	mvprintw(st.y / 2 + 1, st.x / 2 + 5, " |  _ _    _  _ _ ___ ");
	mvprintw(st.y / 2 + 2, st.x / 2 - 27, " \\ V / _ \\ || | / _/ _` | ' \\|/");
	mvprintw(st.y / 2 + 2, st.x / 2 + 2, "|  _| | '  \\/ _ \\ V / -_)");
	mvprintw(st.y / 2 + 3, st.x / 2 - 27, "  |_|\\___/\\_,_| \\__\\__,_|_||_|");
	mvprintw(st.y / 2 + 3, st.x / 2 + 2, " \\__| |_|_|_\\___/\\_/\\___|");
}

int			can_move(int tab[4][4])
{
	int		i;
	int		j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (tab[i][j] == 0 || (j < 3 && tab[i][j] == tab[i][j + 1]) ||
						(j > 0 && tab[i][j] == tab[i][j - 1]) ||
						(i > 0 && tab[i][j] == tab[i - 1][j]) ||
						(i < 3 && tab[i][j] == tab[i + 1][j]))
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}
